export enum Field {
    CLOUD= 'cloud',
    DATA = 'data',
    PHYSICS = 'physics',
    MOBILE= 'mobile',
    WEB3 = 'web3',
    IOT = 'iot',
}

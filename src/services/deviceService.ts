export class DeviceService {
    isMobile() {
        return window.innerWidth <= 900
    }

    unitySize(times: number = 1): string {
        return `${this.isMobile() ? times * 200 /12 : times * 100 /12}%`;
    }
}
